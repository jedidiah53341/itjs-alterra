public class Problem1 {
    
    public static void problem1(String input) {
          int totalKarakter = 0;
          int jumlahVokal = 0;
          int jumlahKonsonan = 0;
          int adaVokal = 0;
          int adaKonsonan = 0;
          char[] daftarVokal = {'A','I','U','E','O','a','i','u','e','o'};
          
          char[] arrayInput = input.toCharArray();
          
          for (char huruf : arrayInput) {

            adaVokal = 0;
            adaKonsonan = 0;
              for (char hurufVokal : daftarVokal) {
                  if (huruf == hurufVokal) {
                      adaVokal = 1;
                  } else if ( huruf == ' ') {
                      adaKonsonan = 0;
                  } else {
                      adaKonsonan = 1;
                  }
              }
            if (adaVokal == 1) {
                jumlahVokal += 1;
                totalKarakter += 1;
            } else if (adaKonsonan == 1 ) {
                jumlahKonsonan += 1;
                totalKarakter += 1;
            }
          }
          System.out.println("Jumlah Vokal : " + jumlahVokal);
          System.out.println("Jumlah Konsonan : " +jumlahKonsonan);
          System.out.println("Total Karakter : " +totalKarakter);
      }
      
    public static void main(String args[]) {
     
      problem1("Alterra Indonesia");


    }
}