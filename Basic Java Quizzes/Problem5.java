public class Problem5 {
    
    public static void problem5(String input) {
        char[] arrayInputAwal = input.toCharArray();
        char[] arrayInputTerbalik = new char[arrayInputAwal.length];
        
        int indeksAwal = arrayInputAwal.length-1;
        int indeksTerbalik = 0;
        while (indeksAwal >= 0) {
            arrayInputTerbalik[indeksTerbalik] = arrayInputAwal[indeksAwal];
            indeksAwal -= 1;
            indeksTerbalik +=1;
        }
        
        boolean merupakanPalindrome = true;
        for (int i=0;i<arrayInputAwal.length;i++) {
            if (arrayInputAwal[i] != arrayInputTerbalik[i]) {
                System.out.println(input + " bukan merupakan palindrome.");
                merupakanPalindrome = false;
                break;
            }
        }
        if ( merupakanPalindrome == true ) {
            System.out.println(input + " adalah palindrome.");
        }
 

    }
      
    public static void main(String args[]) {
     
      problem5("katak");
      problem5("mister");

    }
}