public class Problem3 {
    
    public static void problem3(int input) {
        int bilangan = 1;
        double pembagian = 0;
        
        while (bilangan <= input) {
            pembagian = input % bilangan;
            if (pembagian == 0) {
                System.out.println(bilangan);
            }
            bilangan += 1;
        }
        System.out.println("\n");
    }
      
    public static void main(String args[]) {
     
      problem3(6);
      problem3(20);

    }
}