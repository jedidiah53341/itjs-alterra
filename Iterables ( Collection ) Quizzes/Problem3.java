public class Problem3 {
    
    public static void problem3(int [] arrayInput, int target) {
        int indeksAngkaPertama = 0;
        int indeksAngkaKedua = 0;
        
        int nilaiAngkaPertama = 0;
        int nilaiAngkaKedua = 0;
        
        boolean samaDenganTarget = false;
        
        while (indeksAngkaPertama<arrayInput.length) {
            nilaiAngkaPertama = arrayInput[indeksAngkaPertama];
            indeksAngkaKedua = 0;
            while (indeksAngkaKedua<arrayInput.length) {
                nilaiAngkaKedua = arrayInput[indeksAngkaKedua];
                if (nilaiAngkaPertama+nilaiAngkaKedua == target) {
                    samaDenganTarget = true;
                    break;
                }
                indeksAngkaKedua += 1;
            }
            if (samaDenganTarget == true) {
                break;
            }
            indeksAngkaPertama += 1;
        }
        int[] arrayIndeksFinal = new int[2];
        arrayIndeksFinal[0] = indeksAngkaPertama;
        arrayIndeksFinal[1] = indeksAngkaKedua;
        
        System.out.println("Solusi :");
        for (int indeks : arrayIndeksFinal) {
            System.out.println(indeks);
        }
        System.out.println("\n");

    }
      
    public static void main(String args[]) {
     
    int[] input1 = {1,2,3,4,6}; 
    int[] input2 = {2,5,9,11}; 
    problem3(input1,6);
    problem3(input2,11);

    }
}