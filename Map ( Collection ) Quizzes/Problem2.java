public class Problem2 {
    
    public static void problem2(String input) {
    String[] strArrayInput = input.split("");  
    int[] daftarAngkaUnik = new int[10];
    int[] daftarAngkaMunculHanyaSekali = new int[10];
    boolean duplikat = false;
    int indeks = 0;
    int counter = 0;
    
    for (String strAngka : strArrayInput) {
        duplikat = false;
        for (int angkaUnik : daftarAngkaUnik) {
            if(Integer.parseInt(strAngka) == angkaUnik) {
                duplikat = true;
            }
        }
        if (duplikat == false) {
            daftarAngkaUnik[indeks] = Integer.parseInt(strAngka);
            indeks +=1;
        }
    }
    
    indeks = 0;
    for (int angkaUnik : daftarAngkaUnik) {
        counter = 0;
        for (String strAngka : strArrayInput) {
            if ( angkaUnik == Integer.parseInt(strAngka)) {
                counter += 1;
            }
        }
        if ( counter == 1 ) {
            daftarAngkaMunculHanyaSekali[indeks] = angkaUnik;
            indeks += 1;
        }
    }
    
    
    int ukuranArrayMergedFix = 0;
    for (int angkaSekali : daftarAngkaMunculHanyaSekali) {
        if (angkaSekali != 0) {
            ukuranArrayMergedFix += 1;
        }
        
    }
    int[] mergedArrayFinal = new int[ukuranArrayMergedFix];
    for (int i=0; i<mergedArrayFinal.length; i++) {
        mergedArrayFinal[i] = daftarAngkaMunculHanyaSekali[i];
    }

    for (int angkaSekali : mergedArrayFinal) {
        System.out.println(angkaSekali);
    }
    
    }
      
    public static void main(String args[]) {
     
    problem2("76523752");
    problem2("1122");

    }
}