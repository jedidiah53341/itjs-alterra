public class Problem8 {
    
    public static void problem8(int input) {
        for (int i=1;i<=input;i++) {
            for(int j=1;j<=input;j++) {
                System.out.print(i * j);
                if(i*j >= 100) {
                System.out.print(" ");
                }
                else if(i*j >= 10) {
                System.out.print("  ");
                } else {
                System.out.print("   ");
                }
                
            }
            System.out.println();
        }
        System.out.println("\n");
    }   
    
      
    public static void main(String args[]) {
      problem8(9);
      problem8(30);
    }
}