public class Problem1 {
    
    public static void problem1(String [] array1, String [] array2) {
        
        String[] mergedArray = new String[array1.length + array2.length];
        boolean duplikat = false;
        int indeks = 0;
        
        for (String item : array1) {
            duplikat = false;
            for (String mergedItem : mergedArray) {
                if (item==mergedItem) {
                    duplikat = true;
                }
            }
            if (duplikat == false ) {
                mergedArray[indeks] = item;
                indeks += 1;
            }
        }

        for (String item : array2) {
            duplikat = false;
            for (String mergedItem : mergedArray) {
                if (item==mergedItem) {
                    duplikat = true;
                }
            }
            if (duplikat == false ) {
                mergedArray[indeks] = item;
                indeks += 1;
            }
        }
        
        int ukuranArrayMergedFix = 0;
        for (String mergedItem : mergedArray) {
            if (mergedItem != null) {
                ukuranArrayMergedFix += 1;
            }
        }
        String[] mergedArrayFinal = new String[ukuranArrayMergedFix];
        for (int i=0; i<mergedArrayFinal.length; i++) {
            mergedArrayFinal[i] = mergedArray[i];
        }
        System.out.println("Merged Array :");
        for (String mergedItem : mergedArrayFinal) {
            System.out.println(mergedItem);
        }
        System.out.println("\n");
      }
      
    public static void main(String args[]) {
        
    String[] input1 = {"kazuya","jin","lee"}; 
    String[] input2 = {"kazuya","feng"}; 
    String[] input3 = {"lee","jin"}; 
    String[] input4 = {"kazuya","panda"}; 
     
    problem1(input1,input2);
    problem1(input3,input4);

    }
}