public class Problem2 {
    
    public static void problem2(String input) {
        int jumlahX = 0;
        int jumlahO = 0;
        
        char[] arrayInput = input.toCharArray();

        for (char huruf : arrayInput) {
            if ( huruf == 'x') {
                jumlahX += 1;
            } else if ( huruf == 'o') {
                jumlahO += 1;
            }
        }
        
        if (jumlahX == jumlahO) {
            System.out.println("true, jumlah x sama dengan jumlah o.");
        } else {
            System.out.println("false, jumlah x beda dengan jumlah o.");
        }

      }
      
    public static void main(String args[]) {
     
      problem2("xoxoxo");

    }
}