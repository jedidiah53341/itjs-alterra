public class Problem5 {
    
    public static void problem5(int [] arrayInput) {
        int[] angkaUnik = new int[10];
        int indeksAngkaUnik = 0;
        boolean duplikat = false;
        
        for(int i=0;i<arrayInput.length;i++) {
            duplikat = false;
            for (int j=0; j<angkaUnik.length;j++) {
                if (arrayInput[i] == angkaUnik[j]) {
                    duplikat = true;
                    break;
                }
            }
            if (duplikat == true) {
                arrayInput[i] = 0;
            } else {
                angkaUnik[indeksAngkaUnik] = arrayInput[i];
                indeksAngkaUnik += 1;
            }
        }
        
        int jumlahDuplikat = 0;
        for(int i=0;i<arrayInput.length;i++) {
            if (arrayInput[i] == 0) {
                jumlahDuplikat += 1;
            }
        }
        
        int[] arrayFinal = new int[arrayInput.length - jumlahDuplikat];

        int indeksArrayFinal = 0;
        for(int i=0;i<arrayInput.length;i++) {
            if (arrayInput[i] != 0) {
                arrayFinal[indeksArrayFinal] = arrayInput[i];
                indeksArrayFinal += 1;
            }
        }

        System.out.println("Solusi :");
        System.out.print("Ukuran array setelah semua duplikat dihapus adalah " + (arrayInput.length  - jumlahDuplikat) + ".\n");
        System.out.println("Angka unik yang tersisa adalah :");
        for (int angka : arrayFinal) {
            System.out.println(angka);
        }
        System.out.println("\n");

            
    }
      
    public static void main(String args[]) {
     
    int[] input1 = {2,3,3,3,6,9,9}; 
    int[] input2 = {2,2,2,11}; 

    problem5(input1);
    problem5(input2);

    }
}