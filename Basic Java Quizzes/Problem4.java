public class Problem4 {
    
    public static void problem4(int input) {
        int bilangan = 2;
        double pembagian = 0;
        boolean terbagi = false;
        
        while (bilangan < input) {
            pembagian = input % bilangan;
            if (pembagian == 0) {
                System.out.println( input + " bukan bilangan prima.");
                terbagi = true;
                break;
            }
            bilangan += 1;
        }
        if (terbagi != true) {
        System.out.println( input + " adalah bilangan prima.");
        }
        System.out.println("\n");
    }
      
    public static void main(String args[]) {
     
      problem4(7);
      problem4(10);

    }
}