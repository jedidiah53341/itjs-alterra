public class Problem4 {
    
    public static void problem4ArrayUnique(int [] arrayPertama, int [] arrayKedua) {
        boolean duplikat = false;
        
        for(int i=0;i<arrayPertama.length;i++) {
            duplikat = false;
            for(int j=0; j<arrayKedua.length;j++) {
                if (arrayPertama[i] == arrayKedua[j]) {
                    duplikat = true;
                    break;
                }
            }
            if (duplikat == true) {
            arrayPertama[i] = 0;
            }
        }
        
        int jumlahDuplikat = 0;
        for(int i=0;i<arrayPertama.length;i++) {
            if (arrayPertama[i] == 0) {
                jumlahDuplikat += 1;
            }
        }
        
        int[] arrayFinal = new int[arrayPertama.length - jumlahDuplikat];
        
        int indeksArrayFinal = 0;
        for(int i=0;i<arrayPertama.length;i++) {
            if (arrayPertama[i] != 0) {
                arrayFinal[indeksArrayFinal] = arrayPertama[i];
                indeksArrayFinal += 1;
            }
        }
        
        System.out.println("Solusi :");
        for (int angka : arrayFinal) 
            {
                System.out.println(angka);
            }
        System.out.println("\n");
            
    }
      
    public static void main(String args[]) {
     
    int[] input1 = {1,2,3,4}; 
    int[] input2 = {1,3,5,10,16}; 
    int[] input3 = {3,8}; 
    int[] input4 = {2,8}; 
    problem4ArrayUnique(input1,input2);
    problem4ArrayUnique(input3,input4);

    }
}