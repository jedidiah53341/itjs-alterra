public class Problem6 {
    
    public static void problem6(int x, int n) {
        
        // (int) Math.pow(x, n));
        
        int result = x;
        for (int exponent = 2; exponent <=n; exponent ++) {
            result = result * x;
        }
    System.out.println(x + " pangkat " + n + " sama dengan " + result);
    }
      
    public static void main(String args[]) {
      problem6(2,3);
      problem6(7,2);
    }
}