public class Problem7 {
    
    public static void problem7(int input) {
        int i, j, row = input;       
        for (i=0; i<row; i++)   
        {  
            for (j=row-i; j>1; j--)   
            {System.out.print(" ");}   
            for (j=0; j<=i; j++ ) 
            {System.out.print("* ");}   
            System.out.println();   
        }   
    }   
    
      
    public static void main(String args[]) {
      problem7(5);
      problem7(7);
    }
}