public class Problem6 {
    
    public static void problem6(int [] arrayInput,int k) {
        int maximumSum =0;
        int sum = 0;
        int i = 0;
        int j = 0;
        while (i<arrayInput.length) {
            j = 0;
            sum = 0;
            while(j<k) {
                if (i+k > arrayInput.length) {
                    break;
                }
                sum += arrayInput[i+j];
                j += 1;
            }
            if (i+k >arrayInput.length) {
                break;
            }
            if (maximumSum < sum ) {
                maximumSum = sum;
            }
            i+= 1;
        }
        System.out.println("Maximum sum :");
        System.out.println(maximumSum);
        System.out.println("\n");
        
    }
      
    public static void main(String args[]) {
     
    int[] input1 = {2,1,5,1,3,2}; 
    int[] input2 = {2,3,4,1,5}; 

    problem6(input1,3);
    problem6(input2,2);

    }
}